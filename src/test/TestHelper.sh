set -e
set -u

quit() {
    local message=$1
    echo $1 >&2
    exit 1
}

curdir=`pwd`
while [[ "`pwd`" != */src && "`pwd`" != '/' ]]; do
    cd ..
done
if [ "`pwd`" != '/' ]; then
    PROJECT_SRC_DIR=`pwd`
else
    quit "Could not find project src directory"
fi
cd "${curdir}"

sourceSystemUnderTest() {
    local systemUnderTestName=$1
    local systemUnderTestPath=`find "$PROJECT_SRC_DIR/main" -type f -name "$systemUnderTestName"`
    [ $? -eq 0 ] || quit "Could not find system under test names $systemUnderTestName"
    source "$systemUnderTestPath"
}

pathOfSystemUnderTest() {
    local systemUnderTestName=$1
    local systemUnderTestPath=`find "$PROJECT_SRC_DIR/main" -type f -name "$systemUnderTestName"`
    [ $? -eq 0 ] || quit "Could not find system under test names $systemUnderTestName"
    chmod u+x "${systemUnderTestPath}"
    SYSTEM_UNDER_TEST_PATH="$systemUnderTestPath"
}



#!/bin/sh
set -e
set -u

source ././../TestHelper.sh
pathOfSystemUnderTest StatusLogger.sh
export TERM=xterm

echo "Ensure PASSED status and message is logged"
actual=`"$SYSTEM_UNDER_TEST_PATH" "PASSED" "Chuck Norris"`
[[ "$actual" == "Chuck Norris"* ]] || quit "Expected the message 'Chuck Norris' to be logged but the actual outout '$actual' did not contain the message"
[[ "$actual" == *"PASSED"* ]] || quit "Expected the status 'PASSED' to be logged but the actual outout '$actual' did not contain the status"


echo "Ensure FAILED status and message is logged"
actual=`"$SYSTEM_UNDER_TEST_PATH" "FAILED" "Chuck Norris"`
[[ "$actual" == "Chuck Norris"* ]] || quit "Expected the message 'Chuck Norris' to be logged but the actual outout '$actual' did not contain the message"
[[ "$actual" == *"FAILED"* ]] || quit "Expected the status 'FAILED' to be logged but the actual outout '$actual' did not contain the status"

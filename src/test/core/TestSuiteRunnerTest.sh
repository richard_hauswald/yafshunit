#!/bin/sh
set -e
set -u

source ././../TestHelper.sh
pathOfSystemUnderTest TestSuiteRunner.pl
pathOfExampleTestSuite="`dirname $0`/exampleTestSuite"



echo "ensure that a missing paramter will be reported and will cause an exit 1"
set +e
actualMessage=`"$SYSTEM_UNDER_TEST_PATH"`
actualExitCode=$?
set -e
[ "$actualMessage" == "Missing parameter. Usage: TestSuiteRunner.pl <test_directory_path>" ] || quit "Expected the error message 'Missing parameter. Usage: TestSuiteRunner.pl <test_directory_path>' instead the message '$actualMessage' was shown"
[ "${actualExitCode}" == "1" ] || quit "Expected exit code 1, isntead i got $actualExitCode"



echo "ensure that path to a non existing file system entry will be reported and will cause an exit 1"
set +e
actualMessage=`"$SYSTEM_UNDER_TEST_PATH" "/non/existing/stuff"`
actualExitCode=$?
set -e
[ "$actualMessage" == "test_directory_path '/non/existing/stuff' does not exist" ] || quit "Expected the error message 'test_directory_path '/non/existing/stuff' does not exist' instead the message '$actualMessage' was shown"
[ ${actualExitCode} -eq 1 ] || quit "Expected exit code 1, isntead i got $actualExitCode"



echo "ensure that path to a existing file instead of a directory will be reported and will cause an exit 1"
set +e
actualMessage=`"$SYSTEM_UNDER_TEST_PATH" "$pathOfExampleTestSuite/ChuckNorrisTest.sh"`
actualExitCode=$?
set -e
[[ "$actualMessage" == "test_directory_path '"* ]]                                   || quit "Expected the error message 'test_directory_path '/Users/richard/devops/yafshunit/src/test/core/exampleTestSuite/ChuckNorrisTest.sh' is not a directory' instead the message '$actualMessage' was shown"
[[ "$actualMessage" == *"exampleTestSuite/ChuckNorrisTest.sh' is not a directory" ]] || quit "Expected the error message 'test_directory_path '/Users/richard/devops/yafshunit/src/test/core/exampleTestSuite/ChuckNorrisTest.sh' is not a directory' instead the message '$actualMessage' was shown"
[ ${actualExitCode} -eq 1 ] || quit "Expected exit code 1, isntead i got $actualExitCode"


echo "running example test suite"
actualMessage=`"$SYSTEM_UNDER_TEST_PATH" "$pathOfExampleTestSuite"`
[[ "$actualMessage" == *"HansWurstTest.sh#testHansWurstVsDevisionByZero"* ]] || quit "Expected test case name 'HansWurstTest.sh#testHansWurstVsDevisionByZero' in output(--\n$actualMessage\n--)"
[[ "$actualMessage" == *"HansWurstTest.sh#testHansWurstVsJesus"* ]] || quit "Expected test case name 'HansWurstTest.sh#testHansWurstVsJesus' in output(--\n$actualMessage\n--)"
[[ "$actualMessage" == *"ChuckNorrisTest.sh#testChuckNorrisVsDevisionByZero"* ]] || quit "Expected test case name 'ChuckNorrisTest.sh#testChuckNorrisVsDevisionByZero' in output(--\n$actualMessage\n--)"
[[ "$actualMessage" == *"ChuckNorrisTest.sh#testChuckNorrisVsJesus"* ]] || quit "Expected test case name 'ChuckNorrisTest.sh#testChuckNorrisVsJesus' in output(--\n$actualMessage\n--)"

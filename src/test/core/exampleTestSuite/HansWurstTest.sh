testHansWurstVsJesus() {
    actual="Jesus can walk on water, but Hans Wurst cannot swim"
    assertEqualsString "Hans must be better than Jesus" "Jesus can walk on water, but Hans Wurst can swim through land" "$actual"
}

testHansWurstVsDevisionByZero() {
    actual="Cannot devide by zero"
    assertEqualsString "Devision by zero should not be possible" "Cannot devide by zero" "$actual"
}

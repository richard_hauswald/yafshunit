testChuckNorrisVsJesus() {
    actual="Jesus can walk on water, but Chuck Norris can swim through land"
    assertEqualsString "Chuck must be better than Jesus" "Jesus can walk on water, but Chuck Norris can swim through land" "$actual"
}

testChuckNorrisVsDevisionByZero() {
    actual="Chuck Norris can divide by zero"
    assertEqualsString "Devision by zero should not be possible" "Cannot devide by zero" "$actual"
}

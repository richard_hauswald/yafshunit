#!/bin/sh
set -e
set -u

source ././../TestHelper.sh

echo "Ensure analyser ignores methods not starting with test"
pathOfTestStub="`dirname $0`/SingleTestFileAnalyserTestStub.sh"
pathOfSystemUnderTest SingleTestFileAnalyser.sh

actual=`"$SYSTEM_UNDER_TEST_PATH" "$pathOfTestStub"`

[ "$actual" == "testChuckNorris;testHansWurst;" ] || quit "Expected the SingleTestFileAnalyser to return 'testChuckNorris;testHansWurst;' instead it returned '$actual'"

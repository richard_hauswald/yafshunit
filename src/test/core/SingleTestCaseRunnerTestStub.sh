testFail() {
    assertEqualsString "error message" "one String" "another String"
}

testSuccess() {
    assertEqualsString "irrelevant" "equalString" "equalString"
}

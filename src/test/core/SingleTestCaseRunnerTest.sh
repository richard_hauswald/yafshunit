#!/bin/sh
set -e
set -u

source ././../TestHelper.sh

pathOfTestStub="`dirname $0`/SingleTestCaseRunnerTestStub.sh"
pathOfSystemUnderTest SingleTestCaseRunner.sh


echo "Ensure runner can run successful test methods"
"$SYSTEM_UNDER_TEST_PATH" "$pathOfTestStub" "testSuccess"


echo "Ensure runner can run failing test methods"
set +e
"$SYSTEM_UNDER_TEST_PATH" "$pathOfTestStub" "testFail"
set -e

echo "Ensure runner returns 0 for successful test methods"
ignored=`"$SYSTEM_UNDER_TEST_PATH" "$pathOfTestStub" "testSuccess"`
actualExitCode=$?
[ "$actualExitCode" == "0" ] || quit "Expected 0 as exit code, but instead I got '$actualExitCode'"


echo "Ensure runner returns 1 for failing test methods"
set +e
ignored=`"$SYSTEM_UNDER_TEST_PATH" "$pathOfTestStub" "testFail"`
actualExitCode=$?
set -e
[ "$actualExitCode" == "1" ] || quit "Expected 1 as exit code, but instead I got '$actualExitCode'"

echo "Ensure runner reports error messages for failing test methods"
set +e
actualOutput=`"$SYSTEM_UNDER_TEST_PATH" "$pathOfTestStub" "testFail"`
set -e
[ "$actualOutput" == 'error message - expected: "one String" but was "another String"' ] || quit "Expected 'error message - expected: \"one String\" but was \"another String\"' as output, but instead I got '$actualOutput'"

#!/bin/sh
set -e
set -u

source ../../TestHelper.sh
sourceSystemUnderTest StringEquals.sh

fail() {
    FAILED="TRUE"
    FAILED_MESSAGE="$1"
}

echo "Ensure equal strings do not cause a call to fail"
FAILED="UNDEF"
assertEqualsString "irrelevant" "chuck norris" "chuck norris"
[ "$FAILED" == "UNDEF" ] || quit "Equal strings caused a call to fail"

echo "Ensure non equal string cause a call to fail"
FAILED="UNDEF"
assertEqualsString "irrelevant" "chuck norris" "hans wurst"
[ "$FAILED" == "TRUE" ] || quit "different strings did not cause a call to fail"

echo "Ensure non equal string cause a call to fail with a proper message"
FAILED="UNDEF"
assertEqualsString "Chuck Norris shall return" "chuck norris" "hans wurst"
[ "$FAILED_MESSAGE" == 'Chuck Norris shall return - expected: "chuck norris" but was "hans wurst"' ] || quit "Expected a different string to cause a call to fail with message 'Chuck Norris shall return - expected: \"chuck norris\" but was \"hans wurst\"', but got '$FAILED_MESSAGE'"

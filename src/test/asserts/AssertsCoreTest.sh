#!/bin/sh
set -e
set -u

source ././../TestHelper.sh
sourceSystemUnderTest AssertsCore.sh

echo "Ensure fail will echo the message to stderr"
set +e
message=`fail "Chuck Norris"`
set -e
[ "Chuck Norris" == "$message" ] || quit "Expected fail to echo the message 'Chuck Norris', instead the ouput was '$message'"

echo "Ensure fail will exit with 1"
set +e
`fail "irrelevant" >/dev/null`
exitCode=$?
set -e
[ $exitCode -eq 1 ] || quit "Expected fail to exit with exit code 1, instead the exist code was '$exitCode'"

assertEqualsString() {
	local message=$1
	local expected=$2
	local actual=$3
	[ "${actual}" == "${expected}" ] || fail "$message - expected: \"${expected}\" but was \"${actual}\""
}

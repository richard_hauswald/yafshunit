#!/bin/bash

if [ ! "$#" -eq 1 ]; then
	echo >&2 "Missing parameter <test_file_path> -> Usage: $1 <test_file_path>"
	exit 1
fi
TEST_FILE=${1}
if [ ! -e ${TEST_FILE} ]; then
	echo >&2 "<test_file_path>(${TEST_FILE}) does not point to an existing file"
	exit 1
fi
if [ ! -f ${TEST_FILE} ]; then
	echo >&2 "<test_file_path>(${TEST_FILE}) exists but it does not point to a file"
	exit 1
fi

. ${TEST_FILE}

typeset -F | awk '{ print $3 }' | grep "^test" | tr '\n' ';'

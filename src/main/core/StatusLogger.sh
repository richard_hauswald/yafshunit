#!/bin/bash

if [ ! "$#" -eq 2 ]; then
	echo >&2 "Missing parameter -> Usage: $1 <PASSED|FAILED> <message>"
	exit 1
fi

RESULT=$1
MESSAGE=$2
if [ "$RESULT" == "PASSED" ]; then
    #GREEN
    COLOR="tput setaf 2"
elif [ "$RESULT" == "FAILED" ]; then
    #RED
    COLOR="tput setaf 1"
else
    echo >&2 "<result> must be either one of 'PASSED' or 'FAILED' but it was '$RESULT'"
	exit 1
fi

echo "$MESSAGE"
#Align right
let RES_COL=`tput cols`-12
tput cuf $RES_COL
tput cuu1
echo -n "["
#turn off all attributes
tput sgr0
#BOLD
tput bold
$COLOR
echo -n "$RESULT"
#turn off all attributes
tput sgr0
echo "]"






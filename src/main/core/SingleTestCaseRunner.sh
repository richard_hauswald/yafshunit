#!/bin/bash
set -e

if [ ! "$#" -eq 2 ]; then
	echo >&2 "Missing parameter -> Usage: $1 <test_file_path> <test_method_name>"
	exit 1
fi


assertsPackagePath="`dirname $0`/../asserts"
if [ ! -d ${assertsPackagePath} ]; then
	echo >&2 "Asserts package directory not found under expected path '$assertsPackagePath'"
	exit 1
fi

assertsCoreLibPath="$assertsPackagePath/AssertsCore.sh"
if [ ! -f ${assertsCoreLibPath} ]; then
	echo >&2 "AssertsCore.sh not found under expected path '$assertsCoreLibPath'"
	exit 1
fi
source "$assertsCoreLibPath"

assertsFunctionsDirectoryPath="$assertsPackagePath/asserts.d"
if [ ! -d ${assertsFunctionsDirectoryPath} ]; then
	echo >&2 "Asserts functions directory not found under expected path '$assertsFunctionsDirectoryPath'"
	exit 1
fi

for assertFunctionFile in "${assertsFunctionsDirectoryPath}"/*; do
	source "$assertFunctionFile"
done


TEST_FILE=${1}
TEST_METHOD=${2}

if [ ! -e ${TEST_FILE} ]; then
	echo >&2 "<test_file_path>(${TEST_FILE}) does not point to an existing file"
	exit 1
fi
if [ ! -f ${TEST_FILE} ]; then
	echo >&2 "<test_file_path>(${TEST_FILE}) exists but it does not point to a file"
	exit 1
fi

source "$TEST_FILE"
testFileDirectory="`dirname \"${TEST_FILE}\"`"
cd "$testFileDirectory"

$TEST_METHOD

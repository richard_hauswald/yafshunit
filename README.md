# yafshunit

WYSIWYG: yafshunit is yet another fucking shunit framework. shunit is a framework used to unit test shell scripts.
yafshunit is doing the same job but it does differ from existing solutions in some important
points:

- additional assertions can be added by adding a shell script that defines the assertion function
- you don't have to source framework files in your unit tests
- unit tests are executed in their parent directory
- it is itself unit and integration tested
- most importantly it is delivering colored output!

A very simple example:
```shell
testChuckNorrisVsJesus() {
    actual="Jesus can walk on water, but Chuck Norris can swim through land"
    assertEqualsString "Chuck must be better than Jesus" "Jesus can walk on water, but Chuck Norris can swim through land" "$actual"
}
```

##Usage
###Conventions
There are only three:

1. test files need to end with Test.sh - so `SingleTestCaseRunnerTest.sh` will be detected as test file and `SingleTestCaseRunnerTestStub.sh` will not
2. test methods need to start with test - so `testChuckNorris` will be detected as test method and `hello_test_World` will not
3. all test files must be located under one common parent directory, but there are no limitations for the directory
structure depth. A valid directory structure of a project could look like this:
```shell
yafshunit
├── README.md
├── build.sh
└── src
    ├── main
    │   ├── asserts
    │   │   ├── AssertsCore.sh
    │   │   └── asserts.d
    │   │       └── StringEquals.sh
    │   └── core
    │       ├── SingleTestFileAnalyser.sh
    │       └── StatusLogger.sh
    └── test
        ├── AllTests.sh
        ├── TestHelper.sh
        ├── asserts
        │   ├── AssertsCoreTest.sh
        │   └── asserts.d
        │       └── StringEqualsTest.sh
        └── core
            ├── SingleTestFileAnalyserTest.sh
            ├── SingleTestFileAnalyserTestStub.sh
            └── StatusLoggerTest.sh
```
###Running your precious tests
```shell
richard@deepthought ~/devops/yafshunit$ yafshunit src/test/core/exampleTestSuite              ✹master ‹ruby-2.1.2@devlab› 13:47:35
src/test/core/exampleTestSuite/ChuckNorrisTest.sh#testChuckNorrisVsDevisionByZero                                      [FAILED]
src/test/core/exampleTestSuite/ChuckNorrisTest.sh#testChuckNorrisVsJesus                                               [PASSED]
src/test/core/exampleTestSuite/HansWurstTest.sh#testHansWurstVsDevisionByZero                                          [PASSED]
src/test/core/exampleTestSuite/HansWurstTest.sh#testHansWurstVsJesus                                                   [FAILED]
richard@deepthought ~/devops/yafshunit$                                                       ✹master ‹ruby-2.1.2@devlab› 13:47:47
```

###A very simple test file
```shell
testChuckNorrisVsJesus() {
    actual="Jesus can walk on water, but Chuck Norris can swim through land"
    assertEqualsString "Chuck must be better than Jesus" "Jesus can walk on water, but Chuck Norris can swim through land" "$actual"
}
```

## Installation
###Downloading as tarball and be done with it
1. create a download directory
2. Download https://bitbucket.org/richard_hauswald/yafshunit/downloads/yafshunit_0.1.tar.gz
3. Download https://bitbucket.org/richard_hauswald/yafshunit/downloads/yafshunit_0.1.tar.gz.asc
4. Verify the gpg signature
5. If the signature checks out valid extract to wherever you need it
6. Create an alias yafshunit pointing to the file TestSuiteRunner.pl in your installation directory

Example output:
```shell
richard@deepthought ~/devops$ mkdir yafshunit_install_example                                                        ‹ruby-2.1.2@devlab› 13:28:58
richard@deepthought ~/devops$ cd yafshunit_install_example                                                           ‹ruby-2.1.2@devlab› 13:29:00
richard@deepthought ~/devops/yafshunit_install_example$ wget https://bitbucket.org/richard_hauswald/yafshunit/downloads/yafshunit_0.1.tar.gz
--2015-02-15 13:29:07--  https://bitbucket.org/richard_hauswald/yafshunit/downloads/yafshunit_0.1.tar.gz
Resolving bitbucket.org... 131.103.20.167, 131.103.20.168
Connecting to bitbucket.org|131.103.20.167|:443... connected.
HTTP request sent, awaiting response... 302 FOUND
Location: https://bbuseruploads.s3.amazonaws.com/richard_hauswald/yafshunit/downloads/yafshunit_0.1.tar.gz?Signature=xRsdzrrGITLTJQQ7ZibIGSvtxsQ%3D&Expires=1424005024&AWSAccessKeyId=0EMWEFSGA12Z1HF1TZ82 [following]
--2015-02-15 13:29:08--  https://bbuseruploads.s3.amazonaws.com/richard_hauswald/yafshunit/downloads/yafshunit_0.1.tar.gz?Signature=xRsdzrrGITLTJQQ7ZibIGSvtxsQ%3D&Expires=1424005024&AWSAccessKeyId=0EMWEFSGA12Z1HF1TZ82
Resolving bbuseruploads.s3.amazonaws.com... 54.231.17.97
Connecting to bbuseruploads.s3.amazonaws.com|54.231.17.97|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 2038 (2.0K) [application/x-tar]
Saving to: 'yafshunit_0.1.tar.gz'

100%[========================================================================================================>] 2,038       --.-K/s   in 0s

2015-02-15 13:29:09 (43.2 MB/s) - 'yafshunit_0.1.tar.gz' saved [2038/2038]

richard@deepthought ~/devops/yafshunit_install_example$ wget https://bitbucket.org/richard_hauswald/yafshunit/downloads/yafshunit_0.1.tar.gz.asc
--2015-02-15 13:29:13--  https://bitbucket.org/richard_hauswald/yafshunit/downloads/yafshunit_0.1.tar.gz.asc
Resolving bitbucket.org... 131.103.20.167, 131.103.20.168
Connecting to bitbucket.org|131.103.20.167|:443... connected.
HTTP request sent, awaiting response... 302 FOUND
Location: https://bbuseruploads.s3.amazonaws.com/richard_hauswald/yafshunit/downloads/yafshunit_0.1.tar.gz.asc?Signature=3M7zahQ9uIC%2F9A2tv8ZyQu21ZZE%3D&Expires=1424004984&AWSAccessKeyId=0EMWEFSGA12Z1HF1TZ82 [following]
--2015-02-15 13:29:13--  https://bbuseruploads.s3.amazonaws.com/richard_hauswald/yafshunit/downloads/yafshunit_0.1.tar.gz.asc?Signature=3M7zahQ9uIC%2F9A2tv8ZyQu21ZZE%3D&Expires=1424004984&AWSAccessKeyId=0EMWEFSGA12Z1HF1TZ82
Resolving bbuseruploads.s3.amazonaws.com... 54.231.17.97
Connecting to bbuseruploads.s3.amazonaws.com|54.231.17.97|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 861 [text/plain]
Saving to: 'yafshunit_0.1.tar.gz.asc'

100%[========================================================================================================>] 861         --.-K/s   in 0s

2015-02-15 13:29:14 (27.4 MB/s) - 'yafshunit_0.1.tar.gz.asc' saved [861/861]

richard@deepthought ~/devops/yafshunit_install_example$ gpg --verify yafshunit_0.1.tar.gz.asc yafshunit_0.1.tar.gz   ‹ruby-2.1.2@devlab› 13:29:14
gpg: Signature made Sun Feb 15 12:29:09 2015 CET using RSA key ID 10779AB4
gpg: Good signature from "Richard Hauswald (The Coder) <richard.hauswald@googlemail.com>"
richard@deepthought ~/devops/yafshunit_install_example$ tar -xzf yafshunit_0.1.tar.gz                                ‹ruby-2.1.2@devlab› 13:45:00
richard@deepthought ~/devops/yafshunit_install_example$ alias yafshunit="~/devops/yafshunit_install_example/yafshunit/core/TestSuiteRunner.pl"
richard@deepthought ~/devops/yafshunit_install_example$ yafshunit                                                    ‹ruby-2.1.2@devlab› 13:45:48
Missing parameter. Usage: TestSuiteRunner.pl <test_directory_path>
richard@deepthought ~/devops/yafshunit_install_example$                                                         1 ↵  ‹ruby-2.1.2@devlab› 13:45:52
```

###Include in a git project
TBD


##Further Documentation
There are tests ;) - go and have a look. If you are interested in examples have a look at the directory
src/test/core/exampleTestSuite . This test suite is run by the test src/test/core/TestSuiteRunnerTest.sh

###Additional assertions can be added by adding a shell script that defines the assertion function
1. clone
2. create your assertion file in  src/main/asserts/asserts.d/<YOUR_ASSERTION_NAME>.sh
3. test your assertion file in  src/test/asserts/asserts.d/<YOUR_ASSERTION_NAME>Test.sh
4. ensure yafshunit is still working by running the src/test/AllTests.sh script from wherever you want and verify it does
exit with 0
5. if so create a pull request
6. wait for the project dictator to include your changes

##TODO
create release branches so the can be referred by git includes
